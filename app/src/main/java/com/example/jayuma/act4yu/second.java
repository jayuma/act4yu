package com.example.jayuma.act4yu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class second extends AppCompatActivity {
    public Button btn_go;
    public TextView r_count;
    public EditText r_show;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Bundle bundle= getIntent().getExtras();

        int ans= bundle.getInt("count");
        btn_go=(Button) findViewById(R.id.btn_go);
        r_count=(TextView) findViewById(R.id.r_count);
        r_show=(EditText) findViewById(R.id.r_show);

        r_count.setText(ans);

        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str=r_count.getText().toString();
                int a[]=new int [26];
                for (int i=0;i<str.length();i++){
                    if(str.charAt(i)>=65 && str.charAt(i)<=90){
                        a[str.charAt(i)-65]++;
                    }
                    else if(str.charAt(i)>=97&&str.charAt(i)<=122){
                        a[str.charAt(i)-97]++;
                    }
                }
                for(int i=0;i<26;i++){
                    if(a[i]>0){
                        r_show.setText((char)(i+65)+""+a[i]);
                    }
                }

            }
        });
    }
}
