package com.example.jayuma.act4yu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.test.suitebuilder.TestMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    public Button btn_yes;
    public TextView r_vow;
    public TextView r_wow;
    public EditText r_word;
    int ans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_yes = (Button) findViewById(R.id.btn_yes);
        r_wow = (TextView) findViewById(R.id.r_wow);
        r_vow = (TextView) findViewById(R.id.r_vow);
        r_word = (EditText) findViewById(R.id.r_word);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String basa = r_word.getText().toString();
                String hirap = basa.replaceAll("[aeiouAEIOU]", "");
                r_wow.setText(hirap);
                int x = basa.length();
                int y = hirap.length();
                ans = x - y;
                r_vow.setText("" + ans);

                ImageButton i_bn = (ImageButton) findViewById(R.id.i_bn);
                i_bn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, second.class);
                        intent.putExtra("count",ans);
                        startActivity(intent);
                   }



        });

            }

        });
    }
}